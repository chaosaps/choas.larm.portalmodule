﻿using System;
using System.Collections.Generic;
using System.Linq;
using Chaos.Larm.PortalModule.Main.Extensions;
using Chaos.Portal.Core;
using Chaos.Portal.Core.Indexing.View;
using Chaos.Portal.Core.Module;
using EZArchive.Portal.Module;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Data;
using EZArchive.Portal.Module.Data.View;
using Newtonsoft.Json;

namespace Chaos.Larm.PortalModule.Main
{
  public class LarmModule : IModuleConfig
  {
    public void Load(IPortalApplication portalApplication)
    {
      portalApplication.OnModuleLoaded += (sender, args) =>
        {
          var ezArchiveModule = args.Module as EzArchiveModule;

          if (ezArchiveModule == null) return;
          
          portalApplication.MapRoute("/v6/LarmFile", () => new FileExtension(portalApplication));
          portalApplication.AddView(new LarmSearchView(), Context.Config.SearchView, force: true);
        };
    }
  }

  public class LarmSearchView : SearchView
	{
	  public override IList<IViewData> Index(object objectsToIndex)
	  {
			var asset = objectsToIndex as Asset;

			if (asset == null)
				return new List<IViewData>();

			var labels = Context.Repository.Label.Get(asset.Identifier);
			var search = new SearchBuilder().Build(asset);

			var searchViewDatas = new List<IViewData>();

		  if (search.Fields.Any(f => f.Key == "Udsendelsesdato"))
		  {
			  SearchField sf = new SearchField();
			  sf.Key = "Datospænd";
				var start = asset.GetFields().SingleOrDefault(f => f.Id == "StartDate");
			  var end = asset.GetFields().SingleOrDefault(f => f.Id == "EndDate");

			  if (start != null && end != null)
			  {
				  sf.Value = $"{start.Value} - {end.Value}";
			  }
			  else if (start != null)
			  {
				  sf.Value = $"{start.Value} - ";
			  }
			  else if (end != null)
			  {
				  sf.Value = $"- {end.Value}";
			  }
			  
			  search.Fields.Add(sf);
		  }

			searchViewDatas.Add(new LARMSearchViewData {Dto = search, Src = asset, Labels = labels});

		  // todo replace SVD with larm specific version that caches the added search fields too
			return searchViewDatas;
	  }
	}
	
	public class LARMSearchViewData : IViewData
	{
		public LARMSearchViewData()
		{
			Src = new Asset();
			Labels = new Label[0];
		}

		public IEnumerable<KeyValuePair<string, string>> GetIndexableFields()
		{
			yield return UniqueIdentifier;

			foreach (var assetData in Src.Data)
			{
				foreach (var field in assetData.Fields)
				{
					var id = assetData.DataDefinition.Name + "." + field.Key;
					IEnumerable<string> values = new List<string>();

					try
					{
						values = IndexHelper.GetIndexValue(id, field.Value);
					}
					catch (InvalidOperationException)
					{
					}

					foreach (var value in values)
					{
						if (!string.IsNullOrEmpty(value))
							yield return new KeyValuePair<string, string>(IndexHelper.GetIndexKey(id), value);
					}
				}
			}

			foreach (var searchField in Dto.Fields)
			{
				if (!searchField.Key.Contains("Datospænd"))
				{
					var sfd = Context.Config.GetSearchFieldDefinition(searchField.Key);

					var formattedValue = IndexHelper.GetFormattedValue(searchField.Value, sfd.Type);

					yield return
						new KeyValuePair<string, string>(IndexHelper.GetFormattedSearchKey(sfd.Type, searchField.Key),
							formattedValue);

					if(sfd.IsSortable)
						yield return new KeyValuePair<string, string>(IndexHelper.GetFormattedSortKey(sfd.Type, searchField.Key),
							formattedValue);
				}
			}

			foreach (var annotationGroup in Src.Annotations)
				foreach (var annotation in annotationGroup.Annotations)
					foreach (var pair in annotation)
					{
						var key = annotationGroup.Name.Replace(" ", "_").Replace(".", "_");
						key += "_" + pair.Key.Replace(" ", "_").Replace(".", "_");
						yield return new KeyValuePair<string, string>("sm_an_" + key, pair.Value.ToString());
					}

			yield return new KeyValuePair<string, string>("s_document_typeid", Dto.TypeId);

			foreach (var tag in Src.Tags)
				yield return new KeyValuePair<string, string>("sm_tags", tag.ToLower());

			foreach (var label in Labels)
				yield return new KeyValuePair<string, string>("sm_labels", label.Identifier);

			foreach (var projectId in Labels.Select(l => l.ProjectId).Distinct())
				yield return new KeyValuePair<string, string>("sm_projectids", projectId.ToString());
		}

		[JsonIgnore]
		public KeyValuePair<string, string> UniqueIdentifier 
			=> new KeyValuePair<string, string>("Id", Dto.Identifier);

		public Search Dto { get; set; }

		[JsonIgnore]
		public string Fullname 
			=> "SearchViewData";

		[JsonIgnore]
		public Asset Src { get; set; }

		[JsonIgnore]
		public IEnumerable<Label> Labels { get; set; }
	}
}