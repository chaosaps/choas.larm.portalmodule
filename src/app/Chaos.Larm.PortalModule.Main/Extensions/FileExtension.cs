﻿using System;
using System.Diagnostics;
using System.Linq;
using Chaos.Portal.Core;
using Chaos.Portal.Core.Extension;
using Chaos.Portal.v5.Extension.Result;
using EZArchive.Portal.Module.Core;

namespace Chaos.Larm.PortalModule.Main.Extensions
{
  public class FileExtension : AExtension
  {
	  public FileExtension(IPortalApplication portalApplication) : base(portalApplication)
    {
    }

	  public EndpointResult CanAccess(string objectId, string filePath)
    {
      try
      {
        if (Request.IsAnonymousUser) return EndpointResult.Failed();

	      var asset = Context.Repository.Asset.Get(Guid.Parse(objectId));
	      var canAccess = asset.Files.Any(f => f.Destinations.Any(d => d.Url.Contains(filePath)));
	      if (!canAccess)
					canAccess = AllowAccessThroughDomsId(asset, filePath);

	      if(canAccess && asset.TypeId == 25)
	      {
		      var data = asset.Data.SingleOrDefault(d => "TV Arkiv Metadata".Equals(d.Name, StringComparison.InvariantCultureIgnoreCase));

					if(data == null) return EndpointResult.Failed();
					if(!data.Fields.ContainsKey("MajorGenre") || !data.Fields.ContainsKey("MinorGenre")) return EndpointResult.Failed();

		      canAccess = IsAllowedGenre(data) && IsAllowedChannel(data);
	      }

        return new EndpointResult { WasSuccess = canAccess };
      }
      catch (Exception)
      {
        return EndpointResult.Failed();
      }
      
      bool IsAllowedGenre(AssetData data)
      {
	      var majorGenre = data.Fields["MajorGenre"].ToString().ToLower();
	      var minorGenre = data.Fields["MinorGenre"].ToString().ToLower();
		  
	      return majorGenre != "film" || minorGenre == "miniserie" || minorGenre == "thrillerserie" || minorGenre == "thrillerdrama" || minorGenre.StartsWith("krimi");
      }
      
      bool IsAllowedChannel(AssetData data)
      {
	      switch (data.Fields["Channel"].ToString().ToLower())
	      {
		      case "tv2 sport":
			      return false;
		      default:
			      return true;
	      }
      }
    }

	  private static bool AllowAccessThroughDomsId(Asset asset, string filePath)
	  {
		  var metadata = asset.Data.SingleOrDefault(a => a.Name == "Radio Arkiv metadata");

		  if (metadata == null) return false;

		  var domsId = metadata.Fields.SingleOrDefault(f => f.Key == "DOMSID").Value;

			if (domsId == null) return false;

			return filePath.Contains(domsId.ToString());
	  }
  }
}