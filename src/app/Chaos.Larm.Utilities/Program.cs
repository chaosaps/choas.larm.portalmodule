﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using CHAOS.Data;
using CHAOS.Data.MySql;

namespace Chaos.Larm.Utilities
{
	internal class Program
	{
		public static void Main(string[] args)
		{
			var gateway =
				new Gateway(
					"Server=mysql01.cpwvkgghf9fg.eu-west-1.rds.amazonaws.com;Database=larm-mcm-dev;Uid=chaos;Pwd=CHAOSpbvu7000;CharSet=utf8mb4;");
			Console.OutputEncoding = Encoding.UTF8;


			//Rename(gateway);
			
		}

		private static void Rename(Gateway gateway)
		{
			foreach (var line in File.ReadAllLines("alllarmrenames.csv", Encoding.UTF8))
			{
				var tabIndex = line.IndexOf("\t");
				var oldFile = line.Substring(0, tabIndex);
				var newFile = line.Substring(tabIndex);
				var oldFilename = oldFile.Substring(oldFile.LastIndexOf("/") + 1);
				var newFolderPath = $"/{newFile.Substring(0, newFile.LastIndexOf("/")).Trim()}/".Replace("\\", "/")
					.Replace("//", "/");
				var newFilename = newFile.Substring(newFile.LastIndexOf("/") + 1);

				var sqlStatement = string.Format("SELECT * FROM File WHERE FileName = '{0}'",
					oldFilename.Replace("'", "\\'").Replace("\"", "\\\""));

				try
				{
					foreach (var result in gateway.ExecuteSqlStatement(sqlStatement))
					{
						Console.WriteLine($"FileId: {result.ID}, FolderPath: {result.FolderPath}, FileName: {result.FileName}");
						Console.WriteLine($"  Path: {result.FolderPath} => {newFolderPath}");
						Console.WriteLine($"  File: {result.FileName} => {newFilename}");
						Console.WriteLine();
					}
				}
				catch (Exception)
				{
					Console.WriteLine(sqlStatement);
				}
			}
		}
	}
}