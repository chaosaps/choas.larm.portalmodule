USE `larm-mcm-dev`;

INSERT INTO
  MetadataSchema
  (GUID, Name, SchemaXML, DateCreated)
VALUES
  (0x00000000000000000000000000000108, 'TV TimeAnnotation','{
     "Name":"Tidslinje",
     "Type":1,
     "TypeId":25,
     "Description":"",
     "Fields":[
        {
           "Id":"StartTime",
           "DisplayName":"StartTime",
           "Type":"String",
           "Required":false,
           "Placeholder":"",
           "Note":null
        },{
           "Id":"EndTime",
           "DisplayName":"EndTime",
           "Type":"String",
           "Required":false,
           "Placeholder":null,
           "Note":null
        },
        {
           "Id":"Title",
           "DisplayName":"Titel",
           "Type":"String",
           "Required":false,
           "Placeholder":null,
           "Note":null
        },
        {
           "Id":"Description",
           "DisplayName":"Beskrivelse",
           "Type":"String",
           "Required":false,
           "Placeholder":null,
           "Note":null
        }
     ]
  }', UTC_TIMESTAMP());


UPDATE
  MetadataSchema
SET
  SchemaXML = '{
      "Name":"Radio LARM metadata",
      "Type":0,
      "TypeId":24,
      "Description":"Create a new asset with metadata",
      "Fields":[
         {
            "Id":"Title",
            "DisplayName":"Titel",
            "Type":"Text",
            "Required":false,
            "Placeholder":"Write a title",
            "Note":null
         },
         {
            "Id":"Description",
            "DisplayName":"Beskrivelse",
            "Type":"String",
            "Required":false,
            "Placeholder":null,
            "Note":null
         },
         {
            "Id":"Genre",
            "DisplayName":"Udgivet Af",
            "Type":"String",
            "Required":false,
            "Placeholder":null,
            "Note":null
         },
         {
            "Id":"Channel",
            "DisplayName":"Kanal",
            "Type":"String",
            "Required":false,
            "Placeholder":null,
            "Note":null
         },
         {
            "Id":"PublicationStartDate",
            "DisplayName":"Starttidspunkt",
            "Type":"Datetime",
            "Required":false,
            "Placeholder":null,
            "Note":null
         },
         {
            "Id":"PublicationEndDate",
            "DisplayName":"Sluttidspunkt",
            "Type":"Datetime",
            "Required":false,
            "Placeholder":null,
            "Note":null
         },
         {
            "Id":"Note",
            "Type":"String",
            "Required":false,
            "Placeholder":null,
            "Note":null
         }
      ]
   }'
WHERE
  GUID = 0x00000000000000000000000000000010;

UPDATE
  MetadataSchema
SET
  SchemaXML = '{
      "Name":"TV LARM metadata",
      "Type":0,
      "TypeId":25,
      "Description":"Create a new asset with metadata",
      "Fields":[
         {
            "Id":"Title",
            "DisplayName":"Titel",
            "Type":"Text",
            "Required":false,
            "Placeholder":"Write a title",
            "Note":null
         },
         {
            "Id":"Description",
            "DisplayName":"Beskrivelse",
            "Type":"String",
            "Required":false,
            "Placeholder":null,
            "Note":null
         },
         {
            "Id":"Genre",
            "DisplayName":"Udgivet Af",
            "Type":"String",
            "Required":false,
            "Placeholder":null,
            "Note":null
         },
         {
            "Id":"Channel",
            "DisplayName":"Kanal",
            "Type":"String",
            "Required":false,
            "Placeholder":null,
            "Note":null
         },
         {
            "Id":"PublicationStartDate",
            "DisplayName":"Starttidspunkt",
            "Type":"Datetime",
            "Required":false,
            "Placeholder":null,
            "Note":null
         },
         {
            "Id":"PublicationEndDate",
            "DisplayName":"Sluttidspunkt",
            "Type":"Datetime",
            "Required":false,
            "Placeholder":null,
            "Note":null
         },
         {
            "Id":"Note",
            "Type":"String",
            "Required":false,
            "Placeholder":null,
            "Note":null
         }
      ]
   }'
WHERE
  GUID = 0x00000000000000000000000000000020;

UPDATE
  Module
SET
  Configuration = '{
       "FacetSettings":[
          {
             "Header":"Facetter",
             "Position":"Left sidebar",
             "Fields":[
                {"Key":"{Document}.TypeId", "Type":0},
                {"Key":"{Search}.Kanal", "Type":0}
             ]
          },{
             "Header":"Date drill down",
             "Position":"Date Modal",
             "Fields":[
                {"Key":"{Search}.Udsendelsesdato", "Type":1}
             ]
          }
       ],
       "AdminGroupId":"dbcf03a7-6347-a942-b470-369b77669059",
       "ContributorGroupId":"00000000-0000-0000-0000-000000000002",
       "UserGroupId":"ae95252a-ed9f-0640-b577-44ee7539c516",
       "SearchDefinition":[{
       "DisplayName":"Titel",
           "Ids":["Radio LARM metadata.Title", "Manuskript Arkiv metadata.Title", "Radiooversigt Arkiv metadata.Title", "Radioavisrapport Arkiv metadata.Title", "Radio Arkiv metadata.Title", "TV Arkiv metadata.Title", "Programoversigt Arkiv metadata.Title"],
           "IsSortable":true
       },{
       "DisplayName":"Kanal",
           "Ids":["Radio LARM metadata.Channel", "Radio Arkiv metadata.Channel", "TV Arkiv metadata.Channel"],
           "IsSortable":false
       },{
       "DisplayName":"Udsendelsesdato",
           "Ids":["Radio LARM metadata.PublicationStartDate", "Radio Arkiv metadata.PublicationStartDate", "Manuskript Arkiv metadata.Date", "Radiooversigt Arkiv metadata.Date", "Radioavisrapport Arkiv metadata.Date", "TV Arkiv metadata.PublicationStartDate", "Programoversigt Arkiv metadata.Date"],
           "IsSortable":true
       },{
       "DisplayName":"DOMSID",
           "Ids":["Radio Arkiv metadata.DOMSID", "TV Arkiv metadata.DOMSID"],
           "IsVisible":false
       }],
       "DefaultSortField":"d_search_udsendelsesdato desc",
       "DisplayNotificationsLevel":"Warning",
       "LoginLevel":"Edit",
       "ArchiveName":"LARM",
       "PageSizes":[10, 20, 50],
       "MaxNumberOfPagesShown":10,
       "AssetTitleKeyWords":["title"],
       "EmbedMode": "OnCrossNavigation",
       "MinimumFacetHitsForEmptySearch": 10,
       "SearchView":"ez-dev-larm-search",
       "UserView":"ez-dev-larm-user",
       "IsSandbox":false,
       "Mcm":{
          "UploadDestinationId":3,
          "ProfileObjectTypeId":55,
          "AssetFolderId":2,
          "UserFolderId":1,
          "ProfileMetadataSchemaId":"00000000-0000-0000-0000-000000000001",
          "AssetMetadataSchemaId":  "00000000-0000-0000-0000-100000000000",
          "AnnotationMetadataSchemaId":"00000000-0000-0000-0000-000000000003"
       },
       "Aws":{
          "AccessKey":"AKIAILPP6LE5PUQ6O6WA",
          "SecretAccessKey":"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",
          "UploadBucket":"larm-upload",
          "PipelineId":"1402401413393-jtapo5",
          "AudioPresets":[
             {
                "Id":"1351620000001-300040",
                "Extension":"mp3",
                "FormatId":6
             }
          ],
          "VideoPresets":[
             {
                "Id":"1351620000001-000020",
                "Extension":"mp4",
                "FormatId":7
             }
          ]
       }
    }'
WHERE
  Name = 'EZ-Archive';